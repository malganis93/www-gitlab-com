---
layout: markdown_page
title: "Developer Relations"
---

The developer relations organization includes the following roles:  

- [Technical writing](/jobs/technical-writer/)  
- [Developer advocacy](/handbook/marketing/developer-relations/developer-advocacy/)  
- [Field marketing](/handbook/marketing/developer-relations/field-marketing/)  
- [Content marketing](/handbook/marketing/developer-relations/content-marketing/)  
